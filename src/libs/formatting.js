export const limitLongStr = str => {
  if (str.length > 100) {
    return `${str.substring(0,97)}...`
  }
  return str
}