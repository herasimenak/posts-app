import { fetchPosts, fetchPost, fetchComments } from '../api'
import action from './action';

export const COMMENTS_REQUEST = 'COMMENTS_REQUEST'
export const COMMENTS_FAILURE = 'COMMENTS_REQUEST'
export const GET_COMMENTS = 'GET_COMMENTS'

export const POSTS_REQUEST = 'POSTS_REQUEST'
export const POSTS_FAILURE = 'POSTS_REQUEST'
export const GET_POSTS = 'GET_POSTS'

export const POST_REQUEST = 'POST_REQUEST'
export const POST_FAILURE = 'POST_REQUEST'
export const GET_POST = 'GET_POST'

export const CHANGE_POSTS_COUNT = 'CHANGE_POSTS_COUNT'
export const CHANGE_COMMENTS_COUNT = 'CHANGE_COMMENTS_COUNT'

export const getPosts = count => async dispatch => {
  dispatch(action(POSTS_REQUEST))
  try {
    const posts = await fetchPosts(count)
    dispatch(action(GET_POSTS, { posts }))
  } catch (err) {
    dispatch({ type: POSTS_FAILURE, payload: err.message })
  }
}

export const getPost = id => async dispatch => {
  dispatch(action(POST_REQUEST))
  try {
    const post = await fetchPost(id)
    dispatch(action(GET_POST, { post }))
  } catch (err) {
    dispatch({ type: POST_FAILURE, payload: err.message })
  }
}

export const getComments = (id, count) => async dispatch => {
  dispatch(action(COMMENTS_REQUEST))
  try {
    const comments = await fetchComments(id, count)
    dispatch(action(GET_COMMENTS, { comments }))
  } catch (err) {
    dispatch({ type: COMMENTS_FAILURE, payload: err.message })
  }
}

export const getFullPost = (id, count) => dispatch => {
  return Promise.all([
    dispatch(getPost(id)),
    dispatch(getComments(id, count))
  ])
}

export const changePostsCount = () => action(CHANGE_POSTS_COUNT)
export const changeCommentsCount = () => action(CHANGE_COMMENTS_COUNT)