const URL = 'https://jsonplaceholder.typicode.com/'

export const fetchPosts = async count => {
  const response = await fetch(`${URL}posts`, { method: 'GET' })

  if (response.ok) {
    const posts = await response.json()
    return posts.slice(-count).reverse()
  }

  const errMessage = await response.text()
  throw new Error(errMessage)
}

export const fetchPost = async id => {
  const response = await fetch(`${URL}posts/${id}`, { method: 'GET' })

  if (response.ok) {
    const posts = await response.json()
    return posts
  }

  const errMessage = await response.text()
  throw new Error(errMessage)
}

export const fetchComments = async (id, count) => {
  const response = await fetch(`${URL}posts/${id}/comments`, { method: 'GET' })
  
  if (response.ok) {
    const comments = await response.json()
    return comments.slice(-count).reverse()
  }

  const errMessage = await response.text()
  throw new Error(errMessage)
}
