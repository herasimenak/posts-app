import React from 'react'
import { Route, Switch } from 'react-router-dom'
import Posts from './containers/Posts'
import Post from './containers/Post'

const App = () => (
  <Switch>
    <Route exact path="/" component={Posts} />
    <Route path="/post-item/:id" component={Post} />
  </Switch>
)

export default App
