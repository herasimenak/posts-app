import React from 'react'

const Container = ({ children, color }) => (
  <main className={`app--${color}`}>
    <div className="container">
      {children}
    </div>
  </main>
)

export default Container