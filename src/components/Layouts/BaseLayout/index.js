import React from 'react'
import { Header } from '../../Global'

const BaseLayout = ({ children }) => (
  <div className="main">
    <Header />
    {children}
  </div>
)

export default BaseLayout