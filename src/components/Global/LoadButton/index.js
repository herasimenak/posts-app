import React, { memo } from 'react'
import SpinLoader from '../Loader'

const Button = memo(props =>(
  <div className="button__wrap">
    {props.isFetching && <SpinLoader />}
    <button
      className="button"
      onClick={props.onClick}
      disabled={props.isFetching}
    >
      Show more
    </button>
  </div>
  )
)

export default Button