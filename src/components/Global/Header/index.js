import React, { memo } from 'react'

const Header = memo(() => (
  <header>
    <div className="header text--blue">
      Posts app
    </div>
  </header>
))

export default Header