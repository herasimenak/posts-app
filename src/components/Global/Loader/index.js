import React from 'react'
import Loader from 'react-loader-spinner'

const SpinLoader = () => (
  <Loader 
    type="Oval"
    color="#40a9ff"
    height={50}	
    width={50}
  />
)

export default SpinLoader