import React, { memo } from 'react'

const CommentCard = memo(props => (
  <div className="comment">
    <div className="comment__name">{props.name}</div>
    <div className="comment__mail">{props.mail}</div>
    <div className="comment__content">{props.body}</div>
  </div>
  )
)

export default CommentCard