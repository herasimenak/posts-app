import React, { memo } from 'react'
import { limitLongStr } from '../../../libs/formatting'

const PostCard = memo(props => (
  <article className="post-card">
    <div
      className="post-card__title"
      onClick={props.onClick}
    >
      {props.title}
    </div>
    <div className="post-card__content">{limitLongStr(props.body)}</div>
  </article>
  )
)

export default PostCard