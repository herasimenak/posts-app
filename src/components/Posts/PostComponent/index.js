import React, { memo } from 'react'

const PostComponent = memo(props => (
  <div className="post__wrap">
    <div className="post__title">{props.title}</div>
    <div className="post__content">{props.body}</div>
  </div>
  )
)

export default PostComponent