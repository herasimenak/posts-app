import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { getFullPost, getComments, changeCommentsCount } from '../../actions'
import { LoadButton, SpinLoader } from '../../components/Global'
import { PostComponent, CommentCard } from '../../components/Posts'
import { BaseLayout, Container } from '../../components/Layouts'

class Post extends Component {
  componentDidMount() {
    this.props.getFullPost(this.props.match.params.id, this.props.count)
  }

  handleCountChange = async () => {
    await this.props.changeCommentsCount()
    this.props.getComments(this.props.match.params.id, this.props.count)
  }

  render() {
    const { post, comments, isFetching } = this.props
    if (isFetching && Object.keys(post).length == 0) {
      return (
        <BaseLayout>
          <Container color="white">
            <div className="posts__header">
              <Link to="/">Return to posts</Link>
            </div>
            <div className="spin__wrap">
              <SpinLoader />
            </div>
          </Container>
        </BaseLayout>
      )
    }
    return (
      <BaseLayout>
          <Container color="white">
            <div className="posts__header">
              <Link to="/">Return to posts</Link>
            </div>
            <PostComponent
              title={post.title}
              body={post.body}
            />
          </Container>
          <Container color="grey">
            <div className="comments__header">
              <span>Responses</span>
            </div>
            {comments.map(comment => (
              <CommentCard
                key={comment.id}
                mail={comment.email}
                name={comment.name}
                body={comment.body}
              />
            ))}
            <LoadButton
              onClick={() => this.handleCountChange()}
              isFetching={isFetching}
            />
        </Container>
      </BaseLayout>
    )
  }
}

const mapStateToProps = ({ posts }) => ({
  post: posts.post,
  comments: posts.comments,
  count: posts.commentsCount,
  isFetching: posts.isFetching,
})

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getFullPost,
      getComments,
      changeCommentsCount,
    },
    dispatch
  )

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Post)