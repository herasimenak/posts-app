import React, { Component } from 'react'
import { push } from 'connected-react-router'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { getPosts, changePostsCount } from '../../actions'
import { PostCard } from '../../components/Posts'
import { LoadButton, SpinLoader }from '../../components/Global'
import { BaseLayout, Container } from '../../components/Layouts'

class Posts extends Component {
  componentDidMount() {
    this.props.getPosts(this.props.count)
  }

  handleCountChange = async () => {
    await this.props.changePostsCount()
    this.props.getPosts(this.props.count)
  }

  render() {
    const { posts, isFetching } = this.props

    if (isFetching && posts.length == 0) {
      return (
        <BaseLayout>
          <Container color="white">
            <div>Posts</div>
            <div className="spin__wrap">
              <SpinLoader />
            </div>
          </Container>
        </BaseLayout>
      )
    }

    return (
      <BaseLayout>
        <Container color="white">
          <div className="posts__header posts__header--big">
            <span>Last posts</span>
          </div>
          {posts && posts.map(post => (
          <PostCard key={post.id}
            onClick={() => this.props.changePage(post.id)}
            title={post.title}
            body={post.body}
          />
          ))}
          <LoadButton
            onClick={() => this.handleCountChange()}
            isFetching={isFetching}
          />
        </Container>
      </BaseLayout>
    )
  }
}

const mapStateToProps = ({ posts }) => ({
  posts: posts.posts,
  count: posts.postsCount,
  isFetching: posts.isFetching,
})

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getPosts,
      changePostsCount,
      changePage: id => push(`/post-item/${id}`)
    },
    dispatch
  )

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Posts)