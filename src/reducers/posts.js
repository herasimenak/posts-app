import {
  POSTS_REQUEST,
  GET_POSTS,
  POSTS_FAILURE,
  POST_REQUEST,
  GET_POST,
  POST_FAILURE,
  COMMENTS_REQUEST,
  GET_COMMENTS,
  COMMENTS_FAILURE,
  CHANGE_POSTS_COUNT,
  CHANGE_COMMENTS_COUNT,
} from '../actions'

const initialState = {
  isFetching: true,
  posts: [],
  post: {},
  comments: [],
  postsCount: 10,
  commentsCount: 3,
  errors: []
}

export default (state = initialState, action) => {
  switch (action.type) {
  case POSTS_REQUEST:
  case POST_REQUEST:
  case COMMENTS_REQUEST:
    return { ...state, isFetching: true }
  case GET_POSTS:
  case GET_POST:
  case GET_COMMENTS:
  case POSTS_FAILURE:
  case POST_FAILURE:
  case COMMENTS_FAILURE: 
    return { ...state, ...action.payload, isFetching: false }
  case CHANGE_POSTS_COUNT:
    return { ...state, postsCount: state.postsCount + 10 }
  case CHANGE_COMMENTS_COUNT:
  return { ...state, commentsCount: state.commentsCount + 10 }
  default:
    return state
  }
}